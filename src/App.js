import React, { Component } from "react"
import { Router, Route, Switch } from "react-router-dom"
import Landing from "./components/Landing"
import SignIn from "./components/SignIn"
import SignUp from "./components/SignUp"
import Profile from "./components/Profile"
import Training from "./components/Training"
import ViewTraining from "./components/ViewTraining"
import { auth, base } from "./utils/base"
import "./App.css"
//import Navbar from "./components/Navbar"

class App extends Component {
  state = {
    user: {},
    loggedIn: false,
    loading: true
  }

  componentDidMount() {
    //this.setState({loading: true})
    auth().onAuthStateChanged(user => {
      //this.setState({ loading: false })
      if (user) {
        console.log("User is in the house")
        this.setState({
          loggedIn: true,
          user: user
        })
        console.log(user)

        base
          .fetch(`users/${user.uid}`, {
            context: this
          })
          .then(data => {
            if (data.uid) {
              this.setState({
                userInfo: data,
                userInfoThere: true,
                loading: false
              })
            } else {
              this.setState({
                userInfo: {},
                userInfoThere: false,
                loading: false
              })
            }
          })
          .catch(err => console.log(err))
      } else {
        console.log("User not around")
        this.setState({
          loggedIn: false,
          user: {},
          userInfo: {},
          userInfoThere: false,
          loading: false
        })
      }
    })
  }

  reGet = () => {
    this.setState({ loading: true })
    base
      .fetch(`users/${this.state.user.uid}`, {
        context: this
      })
      .then(data => {
        if (data.uid) {
          this.setState({
            userInfo: data,
            userInfoThere: true,
            loading: false
          })
        } else {
          this.setState({
            userInfo: {},
            userInfoThere: false,
            loading: false
          })
        }
      })
      .catch(err => console.log(err))
  }

  render() {
    const userStatus = {
      loggedIn: this.state.loggedIn,
      user: this.state.user,
      loading: this.state.loading,
      userInfo: this.state.userInfo,
      userInfoThere: this.state.userInfoThere,
      reGet: this.reGet
    }
    //console.log("UserInfoThere "+this.state.userInfoThere)

    return (
      <div className="App">
        <Switch>
          <Route
            exact
            path="/"
            render={() => <Landing userStatus={userStatus} />}
          />
          <Route
            exact
            path="/signin"
            render={() => <SignIn userStatus={userStatus} />}
          />
          <Route
            exact
            path="/signup"
            render={() => <SignUp userStatus={userStatus} />}
          />
          <Route
            exact
            path="/profile"
            render={() => <Profile userStatus={userStatus} />}
          />
          <Route
          exact
          path="/trainings"
          render={() => <Training userStatus={userStatus} />}
        />
        <Route
        exact
        path="/viewtraining"
        render={() => <ViewTraining userStatus={userStatus} />}
      />
        </Switch>
      </div>
    )
  }
}

export default App
