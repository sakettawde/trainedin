import firebase from 'firebase/app'
import fb from 'firebase'
import Rebase from 're-base'

let app = firebase.initializeApp({
    apiKey: "AIzaSyC1ikrKiKxmKJUFpAp3aiFOtJ46rSbA0Pw",
    authDomain: "trainedin-c2c10.firebaseapp.com",
    databaseURL: "https://trainedin-c2c10.firebaseio.com",
    projectId: "trainedin-c2c10",
    storageBucket: "trainedin-c2c10.appspot.com",
    messagingSenderId: "919573787446"
});

export let base = Rebase.createClass(app.database())
export const ref = firebase.database().ref()
export const auth = fb.auth