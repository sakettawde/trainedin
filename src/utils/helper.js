import { auth, ref } from "../utils/base"

export let signOut = () => {
    auth()
      .signOut()
      .then(function() {
        console.log("Sign-out successful")
      })
      .catch(function(error) {
        console.log("An error happened during sign-out. Here it is: "+error.message)
      })
  }

export let loginWithGoogle = () => {
    console.log("loginWithGoogle clicked")
    let gprovider = new auth.GoogleAuthProvider()

    auth().signInWithRedirect(gprovider)
  }

export let loginWithEmailPassword = (email, pw) => {
    return auth()
      .signInWithEmailAndPassword(email, pw)
      .catch(error => {
        // Handle Errors here.
        var errorCode = error.code
        var errorMessage = error.message
        return `${errorCode} ${errorMessage}`
        // ...
      })
  }

export let signUpWithEmailPassword = (email, pw) => {
    auth()
      .createUserWithEmailAndPassword(email, pw)
      .catch(error => {
        var errorCode = error.code
        var errorMessage = error.message
        console.log(`${errorCode} ${errorMessage}`)
      })
  }

// export let saveUser = user => {
//     return ref
//       .child(`users/${user.uid}`)
//       .set({
//         email: user.email,
//         uid: user.uid
//       })
//       .then(() => user)
//   }

