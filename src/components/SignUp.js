import React, { Component } from "react"
import {
  Header,
  Container,
  Button,
  Icon,
  Dimmer,
  Loader,
  Menu,
  Divider,
  Input
} from "semantic-ui-react"
import { Link, Redirect } from "react-router-dom"
import { loginWithGoogle, signOut, signUpWithEmailPassword } from "../utils/helper"

class SignUp extends Component {
  constructor(props) {
    super(props)

    this.state={
      email: "",
      password: "",
      warning: ""
    }

    this.handleInputChange = this.handleInputChange.bind(this)
  }

  validateEmail = (email) => {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
  
  handleInputChange(event) {
    const target = event.target
    //const value = target.type === 'checkbox' ? target.checked : target.value;
    const value = target.value
    const name = target.name

    this.setState({
      [name]: value
    })
  }

  loginEmail = () => {
    this.setState({warning: ""})
    if(this.validateEmail(this.state.email)){
      signUpWithEmailPassword(this.state.email, this.state.password)
    } else {
      this.setState({warning: "enter a valid email"})
    }
    
  }
  
  render() {
    const loading = this.props.userStatus.loading

    return (
      <div className="App">
        <Container>
          <Menu secondary>
            <Menu.Item>
              <Link to="/">
                <Button>Home</Button>
              </Link>
            </Menu.Item>
          </Menu>
        </Container>
        <Container className="App">
          <Header as="h1">Register Yourself</Header>
          <h4>Sign Up with Google</h4>
          <Button color="google plus" onClick={loginWithGoogle}>
            <Icon name="google plus" />Google
          </Button>
          <br/>
          <h4>Sign Up with Email & Password</h4>
          <Input name='email' value={this.state.email} onChange={this.handleInputChange} placeholder='Email'/>
          <br/><br/>
          <Input type='password' name='password' value={this.state.password} onChange={this.handleInputChange} placeholder='Password'/>
          <br/><br/>
          <p>{this.state.warning}</p>
          <Button primary onClick={this.loginEmail}>Sign Up</Button>
        </Container>
        <Dimmer active={loading}>
          <Loader indeterminate>Searching for user...</Loader>
        </Dimmer>
        {this.props.userStatus.loggedIn && <Redirect to="/"></Redirect>}
      </div>
    )
  }
}

export default SignUp
