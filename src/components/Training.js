import React, { Component } from "react"
import {
  Image,
  Header,
  Container,
  Button,
  Item,
  Segment,
  Modal,
  Icon,
  Form,
  Dimmer,
  Loader,
  Divider,
  Confirm
} from "semantic-ui-react"
import { Redirect, Link } from "react-router-dom"
import Navbar from "./Navbar"
import LoggedIn from "./LoggedIn"
import { base } from "../utils/base"
import DatePicker from "react-datepicker"
import "react-datepicker/dist/react-datepicker.css"

class Training extends Component {
  constructor(props) {
    super(props)

    this.state = {
      trainingName: "",
      trainingCo: "",
      contactName: "",
      contactNumber: "",
      startDate: "",
      endDate: "",
      warning: "",
      trainings: [],
      quickAddModal: false,
      addNewTrainingModal: false, 
      redirectToView: false
    }

    this.handleInputChange = this.handleInputChange.bind(this)
  }

  handleInputChange(event) {
    const target = event.target
    //const value = target.type === 'checkbox' ? target.checked : target.value;
    const value = target.value
    const name = target.name

    this.setState({
      [name]: value
    })
  }

  handleDateChange = date => {
    this.setState({
      startDate: date
    })
  }

  handleDateChange2 = date => {
    this.setState({
      endDate: date
    })
  }

  addNewTrainingModalClose = () => {
    this.setState({
      addNewTrainingModal: false,
      trainingName: "",
      trainingCo: "",
      contactName: "",
      contactNumber: "",
      startDate: "",
      endDate: "",
      warning: "",
      loading: "",
      setupDone: false
    })
  }

  addNewTrainingClick = () => {
    this.setState({ addNewTrainingModal: true })
  }

  addNewTraining = () => {
    console.log("clicked")
    this.setState({ warning: "" })
    const {
      trainingName,
      trainingCo,
      contactName,
      contactNumber,
      startDate,
      endDate
    } = this.state

    if (
      !trainingName ||
      !trainingCo ||
      !contactName ||
      !contactNumber ||
      !startDate ||
      !endDate
    ) {
      this.setState({ warning: "Some input(s) are missing" })
      return
    }

    let dataObj = {
      trainingName,
      trainingCo,
      contactName,
      contactNumber,
      startDate: startDate.format("DD/MM/YYYY"),
      endDate: endDate.format("DD/MM/YYYY")
    }

    base
      .push(`trainings/${this.props.userStatus.user.uid}`, {
        context: this,
        data: dataObj
      })
      .then(data => {
        console.log("training added successfully")
        this.addNewTrainingModalClose()
      })
      .catch(err => {
        console.log("some issue, error: ", err)
      })
  }

  getTrainings = () => {
    if (this.state.setupDone) {
      return
    }
    base
      .fetch(`trainings/${this.props.userStatus.user.uid}`, {
        context: this,
        asArray: true
      })
      .then(data => {
        console.log(data)
        this.setState({
          trainings: data,
          loading: false,
          setupDone: true
        })
      })
      .catch(err => console.log(err))
  }

  quickAddModal = () => {
    this.setState({quickAddModal: true})
  }

  quickAddModalClose = () => {
    this.setState({quickAddModal: false})
  }

  viewTraining = (training) => {
    sessionStorage.training = JSON.stringify(training)
    this.setState({redirectToView: true})
  }

  render() {
    const { userStatus } = this.props

    if (userStatus.loggedIn && !userStatus.loading && !this.state.setupDone) {
      this.getTrainings()
    }

    return (
      <div>
        <LoggedIn userStatus={userStatus} />
        <Navbar activeItem="trainings" userStatus={userStatus} />
        <Container className="App">
          <Header as="h1">Trainings</Header>
          <Button primary onClick={this.addNewTrainingClick}>
            Add New Training
          </Button>
          {this.state.trainings.map(training => (
            <Segment color="red" key={training.key}>
              <Header as="h2">{training.trainingName}</Header>
              <p>Training Company: {training.trainingCo}</p>
              <p>Contact Person Name: {training.contactName}</p>
              <p>Contact Person Number: {training.contactNumber}</p>
              <p>Starting On: {training.startDate}</p>
              <p>Ending On: {training.endDate}</p>
              <Divider/>
              <Button floated="right" primary onClick={()=>this.viewTraining(training)}>View Training</Button>
              <Button floated="right" color="green" onClick={this.quickAddModal}>Quick Add</Button>
              <br/><br/>
            </Segment>
          ))}
          
          <Modal
            size="tiny"
            open={this.state.addNewTrainingModal}
            onClose={this.addNewTrainingModalClose}
          >
            <Header icon="add to calendar" content="Add New Training" />
            <Modal.Content>
              <Form>
                <h4>Enter new training related information below</h4>
                <Form.Input
                  label="Training Name"
                  placeholder="Training Name, eg. Introduction to Angular"
                  name="trainingName"
                  value={this.state.trainingName}
                  onChange={this.handleInputChange}
                />
                <Form.Input
                  label="Training Company"
                  placeholder="Company Name"
                  name="trainingCo"
                  value={userStatus.user.trainingCo}
                  onChange={this.handleInputChange}
                />
                <Form.Input
                  label="Primary Contact Name"
                  placeholder="Contact Person's Name"
                  name="contactName"
                  value={this.state.contactName}
                  onChange={this.handleInputChange}
                />
                <Form.Input
                  label="Primary Contact Number"
                  placeholder="Contact Person's Number"
                  name="contactNumber"
                  value={this.state.contactNumber}
                  onChange={this.handleInputChange}
                />
                <Form.Field>
                  <label>Start Date</label>
                  <DatePicker
                    selected={this.state.startDate}
                    onChange={this.handleDateChange}
                    dateFormat="DD/MM/YYYY"
                    peekNextMonth
                    showMonthDropdown
                    showYearDropdown
                    dropdownMode="select"
                    onFocus={e => e.target.blur()}
                  />
                </Form.Field>
                <Form.Field>
                  <label>End Date</label>
                  <DatePicker
                    selected={this.state.endDate}
                    onChange={this.handleDateChange2}
                    dateFormat="DD/MM/YYYY"
                    peekNextMonth
                    showMonthDropdown
                    showYearDropdown
                    dropdownMode="select"
                    onFocus={e => e.target.blur()}
                  />
                </Form.Field>
              </Form>
              <h4 style={{ color: "red" }}>{this.state.warning}</h4>
            </Modal.Content>
            <Modal.Actions>
              <Button color="red" onClick={this.addNewTrainingModalClose}>
                <Icon name="remove" /> Cancel
              </Button>
              <Button color="green" onClick={this.addNewTraining}>
                <Icon name="checkmark" /> Add Information
              </Button>
            </Modal.Actions>
          </Modal>
          <Modal
          size="tiny"
          open={this.state.quickAddModal}
          onClose={this.quickAddModalClose}
        >
          <Header content="Quick Add Menu (under dev)" />
          <Modal.Content>
          <Button primary>Add a Note</Button><br/><br/>
          <Button primary>Add a Task</Button><br/><br/>
          <Button primary>Add an Expense</Button><br/>
          </Modal.Content>
          </Modal>
          <br/>
          <br/>
        </Container>
        {this.state.redirectToView && <Redirect to="/viewtraining" push/> }
      </div>
    )
  }
}

export default Training
