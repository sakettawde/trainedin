import React, { Component } from "react"
import {
  Image,
  Header,
  Container,
  Button,
  Segment,
  Icon,
  Tab,
  Table,
  Grid
} from "semantic-ui-react"
import { Redirect, Link } from "react-router-dom"
import Navbar from "./Navbar"
import LoggedIn from "./LoggedIn"
import { base } from "../utils/base"
import {
  GoogleMap,
  Marker,
  withScriptjs,
  withGoogleMap
} from "react-google-maps"
import convertDMS from "./latlong"

class ViewTraining extends Component {
  state = {
    training: {}
  }

  componentDidMount() {
    this.setState({ training: JSON.parse(sessionStorage.training) })
  }

  render() {
    const { userStatus } = this.props
    const { training } = this.state
    console.log(this.state)
    return (
      <div>
        <LoggedIn userStatus={userStatus} />
        <Navbar activeItem="trainings" userStatus={userStatus} />
        <Container className="App">
          <Header as="h1">View Training</Header>
          <Segment color="red">
            <Header as="h2">{training.trainingName}</Header>
            <Button floated="right" icon="edit" />
            <p>Training Company: {training.trainingCo}</p>
            <p>Starting On: {training.startDate}</p>
            <p>Ending On: {training.endDate}</p>
          </Segment>
          <Tab
            panes={[
              {
                menuItem: "Details",
                pane: (
                  <Tab.Pane key={1}>
                    <Details training={training} />
                  </Tab.Pane>
                )
              },
              {
                menuItem: "Travel",
                pane: (
                  <Tab.Pane key={2}>
                    <Travel />
                  </Tab.Pane>
                )
              },
              {
                menuItem: "Expenses",
                pane: (
                  <Tab.Pane key={3}>
                    <Expenses />
                  </Tab.Pane>
                )
              },
              {
                menuItem: "Notes",
                pane: (
                  <Tab.Pane key={4}>
                    <Notes />
                  </Tab.Pane>
                )
              }
            ]}
            renderActiveOnly={false}
          />
        </Container>
        <br />
        <br />
      </div>
    )
  }
}

const Details = props => (
  <div>
    <Grid columns={2}>
      <Grid.Row>
        <Grid.Column>
          <Header>Training Company</Header>
          <p>{props.training.trainingCo}</p>
        </Grid.Column>
        <Grid.Column>
        </Grid.Column>
      </Grid.Row>
      </Grid>
      <Grid columns={3}>
      <Grid.Row>
        <Grid.Column>
          <Header>Contact Person Name</Header>
          <p><Icon name="user"/>{props.training.contactName}</p>
        </Grid.Column>
        <Grid.Column>
          <Header>Contact Person Number</Header>
          <p><Icon name="phone"/>{props.training.contactNumber}</p>
        </Grid.Column>
        <Grid.Column>
          <Header>Contact Person Email</Header>
          <p><Icon name="mail"/></p>
        </Grid.Column>
        
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          
        </Grid.Column>
        <Grid.Column>
        
      </Grid.Column>
        <Grid.Column>
          <Button primary>Add additional information</Button>
        </Grid.Column>
      </Grid.Row>
    </Grid>
    <p>*Sample content below</p>
    <Header>Daywise Breakup</Header>
    <Table celled>
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell>Day & Date</Table.HeaderCell>
        <Table.HeaderCell>Schedule</Table.HeaderCell>
        <Table.HeaderCell>Notes</Table.HeaderCell>
      </Table.Row>
    </Table.Header>
    <Table.Body>
      <Table.Row>
        <Table.Cell>Friday, 6th October, 2017</Table.Cell>
        <Table.Cell>9am to 2pm<br/>3pm to 5:30pm</Table.Cell>
        <Table.Cell></Table.Cell>
      </Table.Row>
      <Table.Row>
        <Table.Cell>Saturday, 7th October, 2017</Table.Cell>
        <Table.Cell>9am to 2pm</Table.Cell>
        <Table.Cell />
      </Table.Row>
      <Table.Row>
        <Table.Cell>Monday, 9th October, 2017</Table.Cell>
        <Table.Cell>9am to 2pm</Table.Cell>
        <Table.Cell></Table.Cell>
      </Table.Row>
    </Table.Body>
  </Table>
  <Button floated="right" primary>Modify Schedule</Button>
  <Button color="red">Delete/Archive Training</Button>
  <br/>
  <br/>
  </div>
)

const Travel = () => (
  <div>
    <p>*Sample content</p>
    <MyMapComponent
      maplang={"18.5319246"}
      maplong={"73.8387716"}
      isMarkerShown
      googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDGvoP2uP4_M_z7z2ZlXYxiZTLfKKKYmYA&v=3.exp&libraries=geometry,drawing,places"
      loadingElement={<div style={{ height: `100%` }} />}
      containerElement={<div style={{ height: `300px` }} />}
      mapElement={<div style={{ height: `100%` }} />}
    />
    <br />
    <Button floated="right" primary>
      Add Travel Details
    </Button>
    <br />
    <br />
    <Table celled>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Travel Details</Table.HeaderCell>
          <Table.HeaderCell>Date</Table.HeaderCell>
          <Table.HeaderCell>Ref No.</Table.HeaderCell>
          <Table.HeaderCell>Action</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        <Table.Row>
          <Table.Cell>Flight MUM to BLR</Table.Cell>
          <Table.Cell>4/10/17</Table.Cell>
          <Table.Cell>PNR: AZQPT67</Table.Cell>
          <Table.Cell>
            <Button primary>View</Button>
            <Button primary>Edit</Button>
          </Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell>Cab Bengaluru</Table.Cell>
          <Table.Cell>4/10/17</Table.Cell>
          <Table.Cell />
          <Table.Cell>
            <Button primary>View</Button>
            <Button primary>Edit</Button>
          </Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell>Flight BLR to MUM</Table.Cell>
          <Table.Cell>19/10/17</Table.Cell>
          <Table.Cell>PNR: JKQPT87</Table.Cell>
          <Table.Cell>
            <Button primary>View</Button>
            <Button primary>Edit</Button>
          </Table.Cell>
        </Table.Row>
      </Table.Body>
    </Table>
  </div>
)

const MyMapComponent = withScriptjs(
  withGoogleMap(props => (
    <GoogleMap
      defaultZoom={13}
      defaultCenter={{ lat: parseFloat("18.56"), lng: parseFloat("73.80") }}
    >
      {props.isMarkerShown && (
        <Marker
          position={{
            lat: parseFloat(props.maplang),
            lng: parseFloat(props.maplong)
          }}
          onClick={() =>
            window.open(
              "https://www.google.co.in/maps/place/" +
                convertDMS(parseFloat(props.maplang), parseFloat(props.maplong))
            )}
        />
      )}
    </GoogleMap>
  ))
)

const Expenses = () => (
  <div>
    <p>*Sample Content</p>
    <Button floated="right" primary>
      Add Expense
    </Button>
    <br />
    <br />
    <Table celled>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Type of Expense</Table.HeaderCell>
          <Table.HeaderCell>Description</Table.HeaderCell>
          <Table.HeaderCell>Attachment</Table.HeaderCell>
          <Table.HeaderCell>Amount</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        <Table.Row>
          <Table.Cell>Travel</Table.Cell>
          <Table.Cell>Cab - 4/10/17</Table.Cell>
          <Table.Cell>
            <Icon name="attach" />
          </Table.Cell>
          <Table.Cell>₹ 110</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell>Food</Table.Cell>
          <Table.Cell>Lunch - 5/10/17</Table.Cell>
          <Table.Cell />
          <Table.Cell>₹ 410</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell>Stay</Table.Cell>
          <Table.Cell>Hotel Plaza - 6/07/17</Table.Cell>
          <Table.Cell>
            <Icon name="attach" />
          </Table.Cell>
          <Table.Cell>₹ 3100</Table.Cell>
        </Table.Row>
      </Table.Body>
    </Table>
  </div>
)

const Notes = () => (
  <div>
    <p>*Sample Content</p>
    <Button floated="right" primary>
      Add Note
    </Button>
    <br />
    <br />
    <Segment color="yellow">
      <Header>Ideas for Day 3</Header>
      <Button floated="right" icon="edit" />
      <span style={{ color: "#757575" }}>Last Modified: 11/10/2017 5:13PM</span>
      <p>Multiple ideas come to mind:</p>
      <ul>
        <li>Idea 1</li>
        <li>Some other idea</li>
        <li>idea 3</li>
        <li>Idea 4</li>
      </ul>
    </Segment>
    <Segment color="yellow">
      <Header>Additional Contact Information</Header>
      <Button floated="right" icon="edit" />
      <span style={{ color: "#757575" }}>Last Modified: 10/10/2017 2:22PM</span>
      <p>
        May also reach out to these guys:<br />
        Sean - 555 444 3333<br />
        Jack - 989 898 9898
      </p>
    </Segment>
  </div>
)

export default ViewTraining
