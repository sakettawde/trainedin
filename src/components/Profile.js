import React, { Component } from "react"
import {
  Image,
  Header,
  Container,
  Button,
  Item,
  Segment,
  Modal,
  Icon,
  Form,
  Dimmer,
  Loader,
  Divider,
  Confirm
} from "semantic-ui-react"
import { Link } from "react-router-dom"
import Navbar from "./Navbar"
import LoggedIn from "./LoggedIn"
import { base } from "../utils/base"

class Profile extends Component {
  constructor(props) {
    super(props)

    this.state = {
      addBankModal: false,
      bankName: "",
      accountName: "",
      accountNumber: "",
      ifscCode: "",
      name: "",
      email: "",
      number: "",
      region: "",
      add1: "",
      add2: "",
      city: "",
      country: "",
      zipcode: ""
    }

    this.handleInputChange = this.handleInputChange.bind(this)
  }

  addBankClick = () => {
    this.setState({ addBankModal: true })
  }

  handleBankModalClose = () => {
    this.setState({
      addBankModal: false,
      bankName: "",
      accountName: "",
      accountNumber: "",
      ifscCode: "",
      warning: "",
      showConfirm: false,
      loading: false,
      welcomeModal: false
    })
  }

  addBankYes = () => {
    this.setState({ warning: "", loading: true })

    if (
      !this.state.accountName ||
      !this.state.bankName ||
      !this.state.accountNumber ||
      !this.state.ifscCode
    ) {
      this.setState({ warning: "Some inputs are missing", loading: false })
      return
    }

    base
      .push(`/users/${this.props.userStatus.user.uid}/bankInfo`, {
        context: this,
        data: {
          bankName: this.state.bankName,
          accountName: this.state.accountName,
          accountNumber: this.state.accountNumber,
          ifscCode: this.state.ifscCode
        }
      })
      .then(data => {
        console.log("bank added successfully")
        this.handleBankModalClose()
        this.setState({ loading: false })
        this.props.userStatus.reGet()
      })
      .catch(err => {
        console.log("something went wrong, error:" + err)
        this.setState({ loading: false })
      })
  }

  handleInputChange(event) {
    const target = event.target
    //const value = target.type === 'checkbox' ? target.checked : target.value;
    const value = target.value
    const name = target.name

    this.setState({
      [name]: value
    })
  }

  removeBank = key => {
    this.setState({ loading: true })
    base
      .remove(`/users/${this.props.userStatus.user.uid}/bankInfo/${key}`)
      .then(() => {
        console.log("bank removed successfully")
        this.setState({ loading: false })
        this.props.userStatus.reGet()
      })
      .catch(err => {
        //handle error
        console.log(err)
        this.setState({ loading: false })
      })
  }

  welcomeFormClick = () => {
    this.setState({ welcomeModal: true })
  }

  handleWelcomeModalClose = () => {
    this.setState({ welcomeModal: false,
      name:"",
      number:"",
      add1: "",
      add2: "",
      region: "",
      zipcode: "",
      city: "",
      country: ""
     })
  }

  welcomeUser = () => {
    this.setState({ warning2: "", loading: true })

    if (
      !this.state.name ||
      !this.state.number ||
      !this.state.region ||
      !this.state.add1 ||
      !this.state.country ||
      !this.state.zipcode ||
      !this.state.city
    ) {
      this.setState({ warning2: "Some inputs are missing", loading: false })
      return
    }

    base.post(`/users/${this.props.userStatus.user.uid}`, {
      context: this,
      data: {
        uid:this.props.userStatus.user.uid,
        name:this.state.name,
        email:this.props.userStatus.user.email,
        number:this.state.number,
        add1: this.state.add1,
        add2: this.state.add2,
        region: this.state.region,
        zipcode: this.state.zipcode,
        city: this.state.city,
        country: this.state.country,
        type:"Free Tier"
      }
    }).then(data=>{
      console.log("user data added successfully")
      this.setState({loading:false, welcomeModal:false})
      this.props.userStatus.reGet()
    }).catch(err=>{
      console.log("something went wrong, try again, error: ", err)
    })
  }

  render() {
    const { userStatus } = this.props
    const userInfo = userStatus.userInfo
    let bankInfo

    if (userInfo !== undefined) {
      let arr = []

      for (let key in userInfo.bankInfo) {
        let tempObj = userInfo.bankInfo[key]
        tempObj.key = key
        arr.push(tempObj)
      }

      bankInfo = arr
    }

    //<Navbar activeItem="home"/>
    return (
      <div>
        <LoggedIn userStatus={userStatus} />
        <Navbar activeItem="profile" userStatus={userStatus} />
        <Container className="App">
          <Header as="h1">Profile</Header>
          {userStatus.userInfoThere ? (
            <div>
              <Segment color="blue">
                <Item.Group>
                  <Item>
                    <Item.Image
                      src={userStatus.user.photoURL}
                      size="small"
                      shape="circular"
                    />
                    <Item.Content>
                      <Item.Header>{userInfo.name}</Item.Header>
                      <Item.Description>
                        Email: {userInfo.email}
                      </Item.Description>
                      <Item.Description>
                        Phone: {userInfo.number}
                      </Item.Description>
                      <Item.Extra>User Type: {userInfo.type}</Item.Extra>
                    </Item.Content>
                  </Item>
                </Item.Group>
              </Segment>
              <Segment color="green">
                <Item.Group>
                  <Item>
                    <Item.Content>
                      <Item.Header>Personal/Contact Information</Item.Header>
                      <Button floated="right" icon="edit" />
                      <Item.Description>
                        Email: {userInfo.email}
                      </Item.Description>
                      <Item.Description>
                        Phone: {userInfo.number}
                      </Item.Description>
                      <Item.Description>
                        Address: {userInfo.add1}, {userInfo.add2}
                      </Item.Description>
                      <Item.Description>City: {userInfo.city}</Item.Description>
                      <Item.Description>
                        Region/State: {userInfo.region}
                      </Item.Description>
                      <Item.Description>
                        Country: {userInfo.country}
                      </Item.Description>
                    </Item.Content>
                  </Item>
                </Item.Group>
              </Segment>
              <Segment color="red">
                <Item.Group>
                  <Item>
                    <Item.Content>
                      <Button
                        floated="right"
                        primary
                        onClick={this.addBankClick}
                      >
                        Add Bank
                      </Button>
                      <Item.Header>Banking Information</Item.Header>
                      <br />
                      <br />
                      {userInfo.bankInfo ? (
                        bankInfo.map(bankInfo => (
                          <div key={bankInfo.key}>
                            <Item.Description>
                              Bank Name: {bankInfo.bankName}
                            </Item.Description>
                            <Item.Description>
                              Account Holder Name: {bankInfo.accountName}
                            </Item.Description>
                            <Button
                              floated="right"
                              color="red"
                              icon="remove"
                              onClick={() => this.removeBank(bankInfo.key)}
                            />
                            <Item.Description>
                              Account Number: {bankInfo.accountNumber}
                            </Item.Description>
                            <Item.Description>
                              IFSC Code: {bankInfo.ifscCode}
                            </Item.Description>
                            <Divider />
                          </div>
                        ))
                      ) : (
                        <Item.Description>
                          Please add some bank information
                        </Item.Description>
                      )}
                    </Item.Content>
                  </Item>
                </Item.Group>
              </Segment>
              <br />
              <br />
            </div>
          ) : (
            <div>
              <p>
                Looks like it's your first time here! Help us with some much
                needed information so we can get started
              </p>
              <Button color="green" onClick={this.welcomeFormClick}>
                Welcome form
              </Button>
            </div>
          )}
        </Container>

        <Modal
          size="tiny"
          open={this.state.addBankModal}
          onClose={this.handleBankModalClose}
        >
          <Header icon="money" content="Add a bank" />
          <Modal.Content>
            <Form>
              <Form.Input
                label="Bank name"
                placeholder="Bank name"
                name="bankName"
                value={this.state.bankName}
                onChange={this.handleInputChange}
              />
              <Form.Input
                label="Account Holder Name"
                placeholder="Account Name"
                name="accountName"
                value={this.state.accountName}
                onChange={this.handleInputChange}
              />
              <Form.Input
                label="Account No."
                placeholder="Account No."
                name="accountNumber"
                value={this.state.accountNumber}
                onChange={this.handleInputChange}
              />
              <Form.Input
                label="IFSC Code"
                placeholder="IFSC Code"
                name="ifscCode"
                value={this.state.ifscCode}
                onChange={this.handleInputChange}
              />
            </Form>
            <h4 style={{ color: "red" }}>{this.state.warning}</h4>
          </Modal.Content>
          <Modal.Actions>
            <Button color="red" onClick={this.handleBankModalClose}>
              <Icon name="remove" /> Cancel
            </Button>
            <Button color="green" onClick={this.addBankYes}>
              <Icon name="checkmark" /> Add
            </Button>
          </Modal.Actions>
        </Modal>
        <Modal
          size="tiny"
          open={this.state.welcomeModal}
          onClose={this.handleWelcomeModalClose}
        >
          <Header icon="user circle outline" content="Welcome Onboard" />
          <Modal.Content>
            <Form>
              <h4>Do provide us with some personal & contact information</h4>
              <Form.Input
                label="Full Name"
                placeholder="Full name"
                name="name"
                value={this.state.name}
                onChange={this.handleInputChange}
              />
              <Form.Input
                label="Email ID"
                name="email"
                value={userStatus.user.email}
                disabled
              />
              <Form.Input
                label="Contact Number"
                placeholder="Contact Number"
                name="number"
                value={this.state.number}
                onChange={this.handleInputChange}
              />
              <Form.Input
                label="Address Line 1"
                placeholder="Apt/Building Name, Number"
                name="add1"
                value={this.state.add1}
                onChange={this.handleInputChange}
              />
              <Form.Input
                label="Address Line 2"
                placeholder="Street name, Locality (optional)"
                name="add2"
                value={this.state.add2}
                onChange={this.handleInputChange}
              />
              <Form.Input
                label="City"
                placeholder="City"
                name="city"
                value={this.state.city}
                onChange={this.handleInputChange}
              />
              <Form.Input
                label="State/Region"
                placeholder="State or region"
                name="region"
                value={this.state.region}
                onChange={this.handleInputChange}
              />
              <Form.Input
                label="Zipcode"
                placeholder="Zipcode"
                name="zipcode"
                value={this.state.zipcode}
                onChange={this.handleInputChange}
              />
              <Form.Input
                label="Country"
                placeholder="Country"
                name="country"
                value={this.state.country}
                onChange={this.handleInputChange}
              />
            </Form>
            <h4 style={{ color: "red" }}>{this.state.warning2}</h4>
          </Modal.Content>
          <Modal.Actions>
            <Button color="red" onClick={this.handleWelcomeModalClose}>
              <Icon name="remove" /> Cancel
            </Button>
            <Button color="green" onClick={this.welcomeUser}>
              <Icon name="checkmark" /> Add Information
            </Button>
          </Modal.Actions>
        </Modal>
        <Dimmer active={userStatus.loading || this.state.loading}>
          <Loader />
        </Dimmer>
      </div>
    )
  }
}

export default Profile
