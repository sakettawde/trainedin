import React, { Component } from "react"
import { Menu, Header, Container, Button, Image } from "semantic-ui-react"
import { Link } from "react-router-dom"
import { signOut } from "../utils/helper"

class Navbar extends Component {
  state = {
    activeItem: "home"
  }

  handleItemClick = (e, { name }) => this.setState({ activeItem: name })

  render() {
    // const { activeItem } = this.state

    const { activeItem, userStatus } = this.props

    return (
      <Container>
        <Menu secondary>
          <Link to="/">
            <Menu.Item
            as="span"  
            name="home"
              active={activeItem === "home"}
              onClick={this.handleItemClick}
            />
          </Link>
          <Link to="/profile">
            <Menu.Item
            as="span"
              name="profile"
              active={activeItem === "profile"}
              onClick={this.handleItemClick}
            />
          </Link>
          <Link to="/trainings">
            <Menu.Item
            as="span"
              name="trainings"
              active={activeItem === "trainings"}
              onClick={this.handleItemClick}
            />
          </Link>

          {userStatus.loggedIn ? (
            <Menu.Menu position="right">
              <Menu.Item>
                <Image src={userStatus.user.photoURL} avatar />&nbsp;&nbsp;&nbsp;&nbsp;
                <span>{userStatus.user.displayName}</span>
              </Menu.Item>
              <Menu.Item>
                <Button onClick={signOut} basic color="red">
                  Sign Out
                </Button>
              </Menu.Item>
            </Menu.Menu>
          ) : (
            <Menu.Menu position="right">
              <Menu.Item>
                <Link to="/signin">
                  <Button primary>Sign In</Button>
                </Link>
              </Menu.Item>
            </Menu.Menu>
          )}
        </Menu>
      </Container>
    )
  }
}

export default Navbar
