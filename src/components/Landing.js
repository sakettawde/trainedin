import React, { Component } from "react"
import { Header, Container, Button, Image } from "semantic-ui-react"
import { Link } from "react-router-dom"
import Navbar from "./Navbar"

class Landing extends Component {
    
    state = {

    }

    render() {
    const { userStatus } = this.props
        //<Navbar activeItem="home"/>
      return (
        <div>
        <Navbar activeItem="home" userStatus={this.props.userStatus}/>
          <Container className="App">
          
            <Header as="h1">Trained | In</Header>
        
            <h3>Making everything else about training... effortless</h3>
            {userStatus.loggedIn ? (
              <Link to="/trainings"><Button color="green">Head to your Dashboard</Button></Link>
            ) : (
              <Link to="/signup"><Button color="green">Sign Up</Button></Link>
            )}
              
          <Image src="https://firebasestorage.googleapis.com/v0/b/trainedin-c2c10.appspot.com/o/assets%2Fhome-bg.jpg?alt=media&token=0d27f6c1-b72c-4dfe-be80-a6d4a56c93bf" size='huge' floated='right'/>
        
          </Container>
        </div>
      )
    }
  }


  
  export default Landing