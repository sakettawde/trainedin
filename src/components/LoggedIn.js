import React, { Component } from "react"
import { Redirect } from "react-router-dom"

class LoggedIn extends Component {

    render() {
        const { userStatus } = this.props

        return (
            <div>
            {!userStatus.loggedIn && !userStatus.loading &&
                <Redirect to="/"/>
            }
            </div>
            
        )
    }

}

export default LoggedIn